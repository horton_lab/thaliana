## TODO: let's handle spaces with trim from hdr.base_methods.R
##
## In this R-script, we merge Jana's taxonomic assignments (bacteria and fungi,
## for both leaves and roots) with our old-er formatted QIIME OTU Tables (Qiime 1.3)
##
## Author: mhorto
###############################################################################

rm(list=ls());

## we need to update the otu Tables with the new (May 3rd, 2017) assignments from Silva (16S) and Unite (ITS1);
## Jana: what is the qiime/silva/unite version:
##

###############################################################################
## hard-coded variables... 
## identify the organ and phylogenetic target.
###############################################################################
organ <- "leaf";
phylogeneticTarget <- "ITS";
otuCutoff <- 97;

###############################################################################
## 1.) open the original qiime table
###############################################################################
originalOtuTableLocation <- paste( "/Volumes/projects/thaliana/microbes/data/original/thaliana/", phylogeneticTarget, "/", organ, "/otus", otuCutoff, "/picked_otus", otuCutoff, "/", sep="");
setwd(originalOtuTableLocation);

qiimeOTUTableName <- "sep_tissue.otus.final.txt";
qiimeOTUTable <- read.table(qiimeOTUTableName, header=T, sep="\t", as.is=T, stringsAsFactors=F, skip=1, comment.char=""); ##, row.names=1); 
colnames(qiimeOTUTable)[1] <- "otu_id";

## could use head, tail, table, usw. for possible QC. 


###############################################################################
## 2.) open the output from Silva/Unite...
###############################################################################
newTaxonomicAssignmentDirectory <- "~/projects/microbes/data/original/thaliana/taxonomic_assignments_jana";
setwd(newTaxonomicAssignmentDirectory);

## stopifnot( organ == "leaf" | organ == "root" );
if( organ == "leaf" ){
	organ <- "leaves";

} else if( organ == "root" ){
	organ <- "roots";

} else {
	stop(organ, " is not supported yet.\n");
}

## identify the kingdom
if( phylogeneticTarget == "16S" ){
	kingdom <- "bacteria";

} else if( phylogeneticTarget == "ITS" ){
	kingdom <- "fungi";	

} else {
	stop(phylogeneticTarget, " is not supported yet.\n");
}

## determine which (updated taxonomy) file to use.
newPhylogeneticInformationFileName <- paste("export-taxonomy-", kingdom, "-", organ, ".tsv", sep="");
stopifnot(file.exists(newPhylogeneticInformationFileName));

newPhylogeneticInformation <- read.table(newPhylogeneticInformationFileName, header=T, sep="\t", as.is=T, stringsAsFactor=F);
colnames(newPhylogeneticInformation)[1] <- "otu_id";

## ensure that all of the OTU ids are in each file...
dropouts <- which(!qiimeOTUTable[,"otu_id"] %in%  newPhylogeneticInformation[,"otu_id"]); 
if( length(dropouts) > 0 ){
	stop("There are OTUs missing in the new file.\n");
}

dropouts <- which(!newPhylogeneticInformation[,"otu_id"] %in% qiimeOTUTable[,"otu_id"]);
if( length(dropouts) > 0 ){
	stop("There are OTUs in the phylogenetic file that were NOT in the original file.\n");
}

###############################################################################
## 3.) Merge the two datasets...
## merge operates as a join... left-hand join (all.x=T), right hand join (all.y=T) or an inner join (all=FALSE, which is the default) or an outer join (all=TRUE).
###############################################################################
tmp <- merge(x=qiimeOTUTable, y=newPhylogeneticInformation, by="otu_id");

## Silva/Unite prepend the taxonomic assignments with characters to indicate the taxonomic level; we will remove these now...
if( kingdom == "bacteria" ){
	## The Silva database was used for bacteria;
	## remove the D7-D14 garbage...
	tmp[,"Taxon"] <- gsub(";D_7__;D_8__;D_9__;D_10__;D_11__;D_12__;D_13__;D_14__", "", tmp[,"Taxon"]);
	
	## for some reason, Silva prepends the taxonomic assignments with these weird characters, that we have to delete
	databasePrefix <- paste("D_", 0:6, "__", sep=""); ## Jana: the pipe | lets us do an 'or' in the regex...

} else {
	## The Unite database was used for fungi;
	## for some reason, Unite prepends the taxonomic assignments with characters indicating the level of the
	## taxonomic assignment; we have to delete these to have human-readable taxonomic information.
	databasePrefix <- c("k", "p", "c", "o", "f", "g", "s");
	databasePrefix <- unlist( lapply( databasePrefix, paste, "__", sep="" ));
}

## There are also 
unknowns <- c( "unidentified marine bacterioplankton", "unidentified", "uncultured bacterium", "uncultured soil bacterium", "uncultured microorganism", "Unknown Family"); 
patternsToReplace <- paste( c(databasePrefix, unknowns), sep="", collapse="|");
tmp[,"Taxon"] <- sapply(tmp[,"Taxon"], FUN=gsub, pattern=patternsToReplace, replacement="");

## omit the remaining uncultured sets...
unknowns <- paste( c(";uncultured$", ";uncultured;"), sep="", collapse="|" );
tmp[,"Taxon"] <- sapply(tmp[,"Taxon"], FUN=gsub, pattern=unknowns, replacement="");

## move back to the original directory (with the corresponding fna file, otu table, etc.)
## write out the updated file with the silva/unite assignments, and then prepare the file
## for our working codebase
setwd(originalOtuTableLocation);

newSuffix <- paste( ".", ifelse(kingdom == "bacteria", "rdp", "marta"), "_vs_", ifelse(kingdom == "bacteria", "silva", "unite"), ".txt", sep="");
outputFileName <- gsub(".txt$", newSuffix, qiimeOTUTableName);
write.table( tmp, file=outputFileName, row.names=F, quote=F, sep="\t");

tmp2 <- tmp[,-which(colnames(tmp) %in% c("Consensus_Lineage", "Confidence"))];
outputFileName <- gsub(".txt$", ".2.txt", qiimeOTUTableName);
write.table( tmp2, file=outputFileName, row.names=F, quote=F, sep="\t");

###############################################################################
## in the case of the bacteria, we need to omit/handle other possible contaminants!
## 0.) some of the Unassigneds look like thaliana (dna), so we'll mask those.
## 1.) check the chloroplasts to see if they are possible host-contaminants.
## 2.) check the otu-ids that also have been assigned to Rickettsiales;Mitochondria (weird?!).
###############################################################################
## check the unassigneds to see if they are possible host-contaminants
#unassigneds <- subset( tmp, Taxon == "Unassigned")[,c("otu_id", "Consensus_Lineage", "Taxon")];
#
#chloroplasts <- tmp[grep("Chloroplast", tmp[,"Taxon"]), c("otu_id", "Taxon")];
#
### these Rickettsiales guys might be real, but they're too rare to worry about. The most abundant (otu_id == 8201) has 45 reads in the 16S/root data, with the minimum row count for the top 250 samples being 472
#mitochondrialIndices <- grep("mitoch", ignore.case=T, tmp[,"Taxon"]);
#mitochondria <- tmp[mitochondrialIndices,];
#
#if( organ == "leaves" ){ 
#	sampleIndices <- grep("^L", colnames(tmp));
#
#} else {
#	sampleIndices <- grep("^R", colnames(tmp));
#}
#
#problematicOtu <- 167;
#sort(subset(tmp, otu_id == problematicOtu )[,sampleIndices])
