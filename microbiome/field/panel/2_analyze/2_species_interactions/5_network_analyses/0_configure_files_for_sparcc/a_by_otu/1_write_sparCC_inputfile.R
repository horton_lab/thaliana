## This R-script writes microbial community data in sparcc format:
## 			species in rows, samples in columns
## 
## Author: matt.horton
###############################################################################
## steps:
## 1.) open the bacterial community and the fungal community
## 2.) subset each community to a given size, and order them
## 3.) rename the OTU ids to avoid confusion among the kingdoms.
## 4.) write out the file. We don't need the Consensus_Lineage field; the "otu_id" field can be renamed to "col_names"; text file
rm(list=ls());

require(vegan);
source("/Volumes/projects/thaliana/code/hdr.microbiome_methods.R");

#####################################################################################################################
## variables:
#####################################################################################################################
otuCutoff <- 97;
organ <- "leaf";
minimumNumberOfReads <- 250;
sizeOfEachCommunity <- 250;

{
	############################################################################################################################################
	## BACTERIA DATA
	############################################################################################################################################
	bacterial.obj <- getData(
			cutoff=otuCutoff,
			combined=FALSE,
			marker="16S",
			tissue=organ, 
			minReadsPerSite=minimumNumberOfReads, 
			minSamplePerOTU=1, 
			whetherToGetTaxa=TRUE, 
			rarefy=TRUE,
			numberOfRarefactions=1);

	############################################################################################################################################
	## FUNGAL DATA
	############################################################################################################################################
	fungal.obj <- getData(
			cutoff=otuCutoff,
			combined=FALSE,
			marker="ITS",
			tissue=organ, 
			minReadsPerSite=minimumNumberOfReads, 
			minSamplePerOTU=1, 
			whetherToGetTaxa=TRUE, 
			rarefy=TRUE,
			numberOfRarefactions=1);

	outputDirectory <- paste("~/projects/microbes/data/original/thaliana/both/otus", otuCutoff, "/", organ, "/n", sizeOfEachCommunity, "/", sep="");
	if( !file.exists(outputDirectory)){
		dir.create(outputDirectory); #.
		cat("directory created.\n");
	}

	setwd(outputDirectory);

	## get the datasets
	bacteria <- bacterial.obj$data;
	fungi <- fungal.obj$data;

	## reduce the datasets to the same samples...
	overlap <- intersect(colnames(bacteria), colnames(fungi));
	bacteria <- bacteria[,overlap];
	fungi <- fungi[,overlap];
	stopifnot(sum(colnames(bacteria) == colnames(fungi)) == ncol(bacteria)); # this is by def, so no worries here

	## order the datasets based on sequencing effort
	bacteria <- bacteria[order(rowSums(bacteria), decreasing=T),];
	fungi <- fungi[order(rowSums(fungi), decreasing=T),];

	## change the OTU ids to avoid confusion between the kingdoms
	rownames(bacteria) <- do.call(rbind, strsplit(rownames(bacteria) , "_"))[,1];
	rownames(fungi) <- do.call(rbind, strsplit(rownames(fungi) , "_"))[,1];

	bacteria <- cbind(col_names=paste("B_", rownames(bacteria), sep=""), bacteria);
	fungi <- cbind(col_names=paste("F_", rownames(fungi), sep=""), fungi);

	if( sizeOfEachCommunity ){
		bacteria <- bacteria[1:sizeOfEachCommunity,];
		fungi <- fungi[1:sizeOfEachCommunity,];
		outputFileName <- paste(organ, ".", minimumNumberOfReads, ".combined_community_sparcc.n", sizeOfEachCommunity, ".txt", sep="");

	} else {
		outputFileName <- paste(organ, ".", minimumNumberOfReads, ".combined_community_sparcc.AllOTUs.txt", sep="");
	}

	totalCommunity <- rbind(bacteria, fungi);
	write.table(totalCommunity, outputFileName, quote=F, sep="\t", row.names=F, col.names=T);
}
