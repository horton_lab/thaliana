## This R-script uses pca to analyze subsets of the (leaf|root) microbiome
## across a grid, in order to investigate whether analyses of the combined
## bacterial and fungal communities are more powerful than analyzing either
## kingdom separately.
## 
## Author: matt.horton
###############################################################################

rm(list=ls());
source("/Volumes/projects/thaliana/code/hdr.microbiome_methods.R");
require(vegan); require(parallel);

{
	#####################################################################################################################
	## variables:
	#####################################################################################################################
	numberOfCores <- 1;

	otuCutoff <- 97;
	organ <- "root";
	analysisApproach <- "pca";
	normalizationMethod16S <- "hlgr"; #sqrt, pa
	normalizationMethodITS <- "hlgr";
	eigenvectors <- 1:10;

	minReadsBacteria <- 1;
	minReadsFungi <- 1;
	otuThreshold <- 2;

	############################################################################################################################################
	## BACTERIAL data
	############################################################################################################################################
	bacteria <- getData(
			cutoff=otuCutoff,
			combined=FALSE,
			marker="16S",
			tissue=organ, 
			minReadsPerSite=minReadsBacteria, 
			minSamplePerOTU=1, 
			whetherToGetTaxa=TRUE, 
			rarefy=(minReadsBacteria > 1),
			numberOfRarefactions=1)$data;

	############################################################################################################################################
	## FUNGAL data
	############################################################################################################################################
	fungi <- getData(
			cutoff=otuCutoff,
			combined=FALSE,
			marker="ITS",
			tissue=organ, 
			minReadsPerSite=minReadsFungi, 
			minSamplePerOTU=1, 
			whetherToGetTaxa=TRUE, 
			rarefy=(minReadsFungi > 1),
			numberOfRarefactions=1)$data;

	############################################################################################################################################
	## eliminate rare OTUs with a threshold...
	############################################################################################################################################
	bacteria <- applyThreshold(bacteria, otuCountThreshold=otuThreshold);
	fungi <- applyThreshold(fungi, otuCountThreshold=otuThreshold);
	############################################################################################################################################

	cat("There are:", nrow(bacteria), "bacterial OTUs\n");
	cat("surveyed in:", ncol(bacteria), "samples\n");
	cat("There are:", nrow(fungi), "fungal OTUs\n");
	cat("surveyed in:", ncol(fungi), "samples\n");

	cutoffsForBacteria <- c(0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.5, 1);
	cutoffsForFungi <- c(0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.5, 1);
	cutoffs <- expand.grid(cutoffsForBacteria, cutoffsForFungi);
	colnames(cutoffs) <- c("slice_bacteria", "slice_fungi");

	numbersToTestBacteria <- round(nrow(bacteria) * cutoffs[,"slice_bacteria"]);
	numbersToTestFungi <- round(nrow(fungi) * cutoffs[,"slice_fungi"]);
	cutoffs <- cbind(cutoffs, numbers_bacteria=numbersToTestBacteria, numbers_fungi=numbersToTestFungi);

	args <- cbind(cutoffs, index=1:nrow(cutoffs)); # subset of SNPs, based on window size chosen earlier (w/ prep_glmm.mapping.R)

	argList <- apply(args, 1, function(x){ 
				list( 
						analysis_approach = analysisApproach,
						proportion_bacteria = as.numeric(x[1]),
						proportion_fungi = as.numeric(x[2]),
						number_bacterial_species = as.numeric(x[3]),
						number_fungal_species = as.numeric(x[4]),
						index = as.numeric(x[5]))
			});

	setwd(paste("/Volumes/projects/thaliana/microbes/data/original/thaliana/both/", sep=""));

	############################################################################################################################################
	## PCA, CA, or DCA
	############################################################################################################################################
	multicoreAnalysis <- function( arg_set, bacteria, fungi ){

		results <- list();
		cutoff_bacteria <- arg_set$proportion_bacteria;
		cutoff_fungi <- arg_set$proportion_fungi;
		nspecies_bacteria <- arg_set$number_bacterial_species;
		nspecies_fungi <- arg_set$number_fungal_species;
		analysis_approach <- arg_set$analysis_approach;
		index <- arg_set$index;

		cat("Shrinking the bacterial table to:", nspecies_bacteria, "species.\n");
		bac <- bacteria[1:nspecies_bacteria,];
		cat("Shrinking the fungal table to:", nspecies_fungi, "species.\n");
		fun <- fungi[1:nspecies_fungi,];

		community <- rbind(bac, fun);

		indices <- which(colSums(community)==0);
		if( length(indices) > 0 ){
			cat("Omitting sample... dropouts.\n");
			community <- community[,-indices];
		}

		## now exclude species that are missing
		indices <- which(rowSums(community) == 0);
		if( length(indices) > 0 ){
			cat("Omitting species dropouts.\n");
			community <- community[-indices,];
		}

		cat("dimension:", dim(community), "\n");
		################################################################################################################################################
		## determine factors
		################################################################################################################################################
		ecotypeIds <- as.factor(determineSamples(community));
		blocks <- as.factor(getBlockData(community));

		if( analysis_approach == "pca" ){
			data.pca <- rda( t(community), scale=T); # SET scale = TRUE FOR CORRELATION.
			data.ef <- envfit( data.pca ~ blocks + ecotypeIds, permutations=999, choices=eigenvectors);

		} else if( analysis_approach == "dca" ){
			data.dca <- decorana( t( community ));
			data.ef <- envfit( data.dca ~ blocks + ecotypeIds, permutations=999, choices=eigenvectors);

		} else if( analysis_approach == "ca" ){
			data.cca <- cca( t(community) );
			data.ef <- envfit( data.cca ~ blocks + ecotypeIds, permutations=999, choices=eigenvectors);

		} else {
			stop("Not supported.\n");
		}

		results <- c(test=analysis_approach, number_of_eigenvectors=length(eigenvectors), proportion_bacteria=cutoff_bacteria, proportion_fungi=cutoff_fungi, 
							nspecies_bacteria=nspecies_bacteria, nspecies_fungi=nspecies_fungi, data.ef[2]$factors$pvals );

		cat(analysisApproach, "permutation-results for index:", index," completed.\n");
		return(results);
	}

	cat("Distributing", analysisApproach, "jobs.\n");
	bacteria.nmzd <- normalizeInSomeWay( bacteria, normalizationMethod=normalizationMethod16S );	
	fungi.nmzd <- normalizeInSomeWay( fungi, normalizationMethod=normalizationMethodITS );

	names <- intersect(colnames(bacteria.nmzd), colnames(fungi.nmzd));
	bacteria.nmzd <- bacteria.nmzd[order(rowSums(bacteria.nmzd), decreasing=T), names];
	fungi.nmzd <- fungi.nmzd[order(rowSums(fungi.nmzd), decreasing=T), names];
	stopifnot(colnames(bacteria.nmzd) == colnames(fungi.nmzd));

	resultSet <- mclapply(
			argList,
			multicoreAnalysis,
			bacteria=bacteria.nmzd,
			fungi=fungi.nmzd,
			mc.cores=numberOfCores ); ## returns: a list of lists...

	resultSet <- do.call(rbind, resultSet);

	############################################################################################################################################
	## write the pca results
	############################################################################################################################################
	minReads <- paste( unique(c( minReadsBacteria, minReadsFungi)), sep="", collapse="_"); rm(minReadsBacteria); rm(minReadsFungi);
	outputFilename <- paste(
			paste("otus", otuCutoff, "/", sep=""), organ, "/ordination/",
			organ, ".", minReads, ".", normalizationMethod16S, "_16S.", normalizationMethodITS, "_ITS.gridded.", max(eigenvectors), "eigs.tn", otuThreshold, ".", analysisApproach, ".txt", sep="");

	if( !file.exists(dirname(outputFilename))){
		cat("Making directory.\n");
		dir.create(dirname(outputFilename));
	}

	write.table(resultSet, outputFilename, row.names=F, quote=F, sep="\t");
	cat("Wrote output file:", outputFilename, "\n");
}
